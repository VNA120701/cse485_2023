-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: faketube
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channel` (
  `ChannelID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ChannelName` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `SubscribersCount` int(11) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ChannelID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (1,'Vallie Ferry','Mr. Aric Glover',141,'Dr. Amina Will IV','2023-08-18 02:44:25','2023-08-18 02:44:25'),(2,'Hillard Homenick V','Lavon Haley',336,'Dudley Muller','2023-08-18 02:44:25','2023-08-18 02:44:25'),(3,'Abner Hirthe','Gregoria Nicolas DDS',185,'Liliane Price','2023-08-18 02:44:25','2023-08-18 02:44:25'),(4,'Marcus Emard','Dr. Alaina Weimann',122,'Darrin Kunde','2023-08-18 02:44:25','2023-08-18 02:44:25'),(5,'Helen McDermott','Adam Schimmel',462,'Naomie Jast','2023-08-18 02:44:25','2023-08-18 02:44:25'),(6,'Otto McLaughlin III','Jordane Franecki',126,'Jailyn Blanda','2023-08-18 02:44:25','2023-08-18 02:44:25'),(7,'Dario Quigley','Mrs. Alycia Jacobi',189,'Mr. Cruz Mertz Jr.','2023-08-18 02:44:25','2023-08-18 02:44:25'),(8,'Emmy Schuster','Roman Beier',182,'Jevon Gerlach','2023-08-18 02:44:25','2023-08-18 02:44:25'),(9,'Ena Hahn','Zetta Brown',358,'Cynthia Veum','2023-08-18 02:44:25','2023-08-18 02:44:25'),(10,'Brandy West Jr.','Dr. Judson Denesik IV',226,'Carlos Olson','2023-08-18 02:44:25','2023-08-18 02:44:25'),(11,'Estella Marvin','Mrs. Onie Purdy III',348,'http://www.wolf.net/minus-aspernatur-culpa-illo-error-quia-modi-corrupti-rerum','2023-08-18 02:44:49','2023-08-18 02:44:49'),(12,'Elias Langosh','Kody Oberbrunner',472,'http://www.kemmer.net/ad-a-quo-id-dolores-est-culpa-et','2023-08-18 02:44:49','2023-08-18 02:44:49'),(13,'Chloe Schuster','Dr. Trystan Will DVM',275,'http://weimann.info/','2023-08-18 02:44:49','2023-08-18 02:44:49'),(14,'Prof. Gaylord Turner','Ada Shields',114,'http://www.stehr.biz/et-at-nihil-facere-architecto-est','2023-08-18 02:44:49','2023-08-18 02:44:49'),(15,'Dr. Noe Keebler V','Miss Leta Macejkovic',463,'http://www.donnelly.info/sunt-id-aut-repellendus-tempore','2023-08-18 02:44:49','2023-08-18 02:44:49'),(16,'Randall Vandervort','Alvena Dooley',156,'http://crooks.info/','2023-08-18 02:44:49','2023-08-18 02:44:49'),(17,'Karina Fahey','Zoie Kemmer',333,'https://skiles.info/facilis-quod-nemo-dolorem-totam.html','2023-08-18 02:44:49','2023-08-18 02:44:49'),(18,'Euna Wolff','Chauncey Mohr',176,'http://www.nitzsche.info/libero-nobis-ipsam-ut-in-sit-dolorem-dolore','2023-08-18 02:44:49','2023-08-18 02:44:49'),(19,'Jacquelyn Morar','Katherine McGlynn DDS',232,'https://www.ziemann.com/fuga-voluptatem-in-porro-quaerat-sunt-perspiciatis-possimus','2023-08-18 02:44:49','2023-08-18 02:44:49'),(20,'Berta Olson I','Nigel Ziemann IV',323,'http://adams.com/maiores-aperiam-perspiciatis-quia-et-est-omnis','2023-08-18 02:44:49','2023-08-18 02:44:49'),(21,'Prof. Unique Armstrong','Miss',224,'http://greenfelder.com/dolores-eos-qui-voluptates-nisi-neque.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(22,'Miss Justina Cassin','Ms.',402,'http://www.schumm.com/incidunt-error-cum-cupiditate-et-est.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(23,'Helene Lind','Mr.',388,'https://brown.biz/pariatur-delectus-fuga-quam-iste-eveniet.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(24,'Alberto Schroeder','Prof.',381,'http://littel.info/','2023-08-18 02:45:27','2023-08-18 02:45:27'),(25,'Constantin Cole V','Miss',303,'https://www.kassulke.com/est-consequatur-repudiandae-quisquam-quae-quisquam-et-fugiat','2023-08-18 02:45:27','2023-08-18 02:45:27'),(26,'Marlene Murazik I','Ms.',357,'http://www.bins.org/','2023-08-18 02:45:27','2023-08-18 02:45:27'),(27,'Luigi Abernathy','Dr.',135,'http://borer.info/in-minus-vel-in-similique-vel.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(28,'Jovanny Lemke','Miss',426,'http://funk.com/facilis-enim-dolores-molestiae-commodi-veniam-neque-sunt.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(29,'Crystal Bosco DVM','Prof.',357,'https://adams.info/nulla-eum-incidunt-repudiandae-et.html','2023-08-18 02:45:27','2023-08-18 02:45:27'),(30,'Emerald Hirthe','Prof.',384,'http://koepp.org/sapiente-consequatur-numquam-delectus-a-sunt-et-fuga','2023-08-18 02:45:27','2023-08-18 02:45:27');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2023_08_18_090517_create_channel_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'faketube'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-18 16:56:10
