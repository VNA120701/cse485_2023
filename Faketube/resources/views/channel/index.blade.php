<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Channel</title>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<body>
{{--<table>--}}
{{--    <thead>--}}
{{--    <tr>--}}
{{--        <th>Channel ID</th>--}}
{{--        <th>Channel Name</th>--}}
{{--        <th>Description</th>--}}
{{--        <th>Subscriber Count</th>--}}
{{--        <th>URL</th>--}}
{{--        <th>Action</th>--}}
{{--    </tr>--}}
{{--    </thead>--}}
{{--    <tbody>--}}
{{--    @foreach($channels as $channel)--}}
{{--        <tr>--}}
{{--            <td>{{ $channel->ChannelID }}</td>--}}
{{--            <td>{{ $channel->ChannelName }}</td>--}}
{{--            <td>{{ $channel->Description }}</td>--}}
{{--            <td>{{ $channel->SubscribersCount }}</td>--}}
{{--            <td>{{ $channel->URL }}</td>--}}
{{--            <td>--}}
{{--                <form action="{{ route('channel.destroy', $channel->ChannelID) }}" method="POST">--}}
{{--                    @csrf--}}
{{--                    @method('DELETE')--}}
{{--                    <button type="submit" class="delete-btn" data-confirm="Are you sure you want to delete this channel?">Delete</button>--}}
{{--                </form>--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--    </tbody>--}}
{{--</table>--}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('success') }}
    </div>
@endif
<div class="container">
    <h2>Channel List</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Channel ID</th>
            <th>Channel Name</th>
            <th>Description</th>
            <th>Subscriber Count</th>
            <th>URL</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($channels as $channel)
            <tr>
                <td>{{ $channel->ChannelID }}</td>
                <td>{{ $channel->ChannelName }}</td>
                <td>{{ $channel->Description }}</td>
                <td>{{ $channel->SubscribersCount }}</td>
                <td>{{ $channel->URL }}</td>
                <td>
                    <form action="{{ route('channel.destroy', $channel->ChannelID) }}" method="POST" class="delete-form">
                        @csrf
                        @method('DELETE')
                        <button type="submit" id="delete-btn" class="btn btn-danger" data-confirm="Are you sure you want to delete this channel?">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
<script>
    const deleteButtons = document.querySelectorAll('.btn.btn-danger');

    deleteButtons.forEach(button => {
        button.addEventListener('click', (event) => {
            const confirmMessage = button.getAttribute('data-confirm');
            if (!confirm(confirmMessage)) {
                event.preventDefault();
            }
        });
    });
</script>
</html>
