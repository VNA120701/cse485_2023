<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class ChannelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */

    public function run(): void
    {
        $fake = Factory::create();

        $limit = 10;
        for ($i = 0; $i < $limit; $i++){
            DB::table('Channel')->insert([
                'ChannelName' => $fake->name,
                'Description' => $fake->sentence,
                'SubscribersCount' => $fake->numberBetween(100,500),
                'URL' => $fake->url(),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
        //
    }
}
